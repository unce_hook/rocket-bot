const bot = require('bbot');
const rp = require('request-promise');
const cheerio = require('cheerio');
const encoding = require('encoding');


let wiki_login_base = 'http://nasredin.intra.ispras.ru/mediawiki/api.php?action=login&lgname=APIBot&lgpassword=ytrewq1&format=json'

function auth() {
  let token = ''
  let options = {
    method: 'POST',
    uri: wiki_login_base,
    jar: true,
    json: true // Automatically stringifies the body to JSON
  };
  return new Promise(function (resolve, reject) {
    rp(options)
      .then(function (response) {
        token = response.login.token;

        // let cookiejar = rp.jar();
        // cookiejar.setCookie(cookie, 'http://nasredin.intra.ispras.ru');

        options.uri = wiki_login_base + '&lgtoken=' + token
        // options.jar = cookiejar

        rp(options)
          .then(function (response) {
            resolve(response.login.result)
          })
      })
  })
}

function get_page_token(title) {
  let options = {
    method: 'GET',
    uri: "http://nasredin.intra.ispras.ru/mediawiki/api.php",
    qs: {
      action: "query",
      prop: "info",
      titles: title,
      intoken: "edit",
      format: "json"
    },
    jar: true,
    json: true // Automatically stringifies the body to JSON
  };
  return new Promise(function (resolve, reject) {
    rp(options)
      .then(function (response) {
        for (let page in response.query.pages) {
          resolve(response.query.pages[page].edittoken)
        }
      })
      .catch(function (err) {
        // POST failed...
        resolve("Error: " + err)
      });
  })
}

function add_link(title, url, url_title) {
  let text = "\n* [" + url + " " + url_title + "]"

  return new Promise(function (resolve, reject) {
    get_page_token(title).then(function (result) {
      let token = result
      // console.log(token)
      let options = {
        method: 'POST',
        uri: "http://nasredin.intra.ispras.ru/mediawiki/api.php",
        formData: {
          action: "edit",
          title: title,
          appendtext: text,
          token: token,
          section: 1,
          contenformat: "text/x-wiki",
          format: "json"
        },
        jar: true,
        json: true // Automatically stringifies the body to JSON
      };
      rp(options)
        .then(function (response) {
          // console.log(response)
          resolve(response.edit.result)
        })
        .catch(function (err) {
          // POST failed...
          resolve("Error: " + err)
        });
    })
  })
}

function add_file(page_title, file_url, file_title) {
  let text = "\n* [" + file_url + " " + file_title + "]"

  return new Promise(function (resolve, reject) {
    get_page_token(page_title).then(function (result) {
      let token = result
      // console.log(token)
      let options = {
        method: 'POST',
        uri: "http://nasredin.intra.ispras.ru/mediawiki/api.php",
        formData: {
          action: "edit",
          title: page_title,
          appendtext: text,
          token: token,
          section: 2,
          contenformat: "text/x-wiki",
          format: "json"
        },
        jar: true,
        json: true // Automatically stringifies the body to JSON
      };
      rp(options)
        .then(function (response) {
          // console.log(response)
          resolve(response.edit.result)
        })
        .catch(function (err) {
          // POST failed...
          resolve("Error: " + err)
        });
    })
  })
}

// bot.global.text(/wiki/, async (b) => {
//   auth().then(function(result){
//     b.respond(result)
//   })
// })

bot.global.direct(/wiki/, async (b) => {
  let msg_args = b.message.toString()
  // console.log(msg_args)
  let count = false
  let url = msg_args.match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9а-эА-Э@:%_\+.~#?&//=]*)/i)[0]
  url = encodeURI(url)
  let options = {
    method: 'GET',
    uri: url,
    gzip: true,
    jar: true,
    resolveWithFullResponse: true,
    encoding: null
  };

  auth().then(function (result) {
    rp(options)
      .then(function (response) {
        let $ = '';
        let charset = 'utf-8';
        console.log(response.headers['content-type']);
        if (response.headers['content-type'].split(';')[1]) {
          charset = response.headers['content-type'].split(';')[1].split("=")[1]
        }
        if (charset.toLowerCase() !== 'utf-8') {
          let resultBuffer = encoding.convert(response.body, 'utf-8', charset);
          $ = cheerio.load(resultBuffer.toString('utf-8'));
        }  else {
          $ = cheerio.load(response.body.toString('utf-8'), {decodeEntities: true});
        }
        let title = $('title').text()
        
        add_link("Интересные_штуки", url, title)
          .then((result) => {
            b.respond("url: " + "http://nasredin.intra.ispras.ru/mediawiki/index.php/Интересные_штуки" +
              "\ntitle: " + title +
              "\nresult: " + result)
          })
      })
  })

})

/**
 * `direct` branch type requires the bot to be explicitly addressed.
 *
 * `reply` instead of `respond` prepends messages with user's name.
 *
 * In Rocket.Chat all messages to a bot in a direct room have the name prepended
 * by the Rocket.Chat SDK before it's processed by the bot framework.
 *
 * Test with "@brocket Hello" or just "Hello" in a DM.
*/
bot.global.direct(/hi|hello/i, (b) => b.reply('Hey there.'), {
  id: 'hello-direct'
})

/**
 * `respondVia` allows using custom platform methods to dispatch response.
 *
 * Matcher conditions allow semantic attributes with a string or array of
 * optional values to match against, possibly capturing content.
 * Accepted atts: is, starts, ends, contains, excludes, after, before, range
 *
 * Test with "Hello anyone?"
*/
bot.global.text({
  contains: ['hi', 'hello']
}, (b) => b.respondVia('react', ':wave:'), {
    id: 'hello-react'
  })


/**
 * Branch callbacks allow asynchronous responding, if they return a promise.
 * State (b) includes branch matching attributes, see bbot.chat/docs/thought.
 *
 * Test with "@brocket ping back in 5 seconds"
*/
bot.global.direct(/ping back in (\d*)/i, async (b) => {
  const ms = parseInt(b.match[1]) * 1000
  await new Promise((resolve) => setTimeout(resolve, ms))
  return b.respond('Ping :ping_pong:')
}, {
    id: 'ping-delay'
  })

/**
 * The `respond` method can accept attachment objects as well as strings.
 * Rendering support depends on the message platform and adapter. In shell,
 * it will display the fallback text.
 *
 * Test with "bot attach image"
 */
bot.global.text(/attach image/i, (b) => {
  return b.respond({
    fallback: `See: https://www.wikiwand.com/en/Three_Laws_of_Robotics`,
    image: `https://upload.wikimedia.org/wikipedia/en/8/8e/I_Robot_-_Runaround.jpg`,
    title: {
      text: `Asimov's Three Laws of Robotics`,
      link: `https://www.wikiwand.com/en/Three_Laws_of_Robotics`
    }
  })
}, { id: 'attach-image' })

/**
 * The `envelope` provides helpers for adding rich-message payloads before
 * responding. Preparing envelopes before dispatch also allows changing the
 * user/room the envelope isaddressed to or dispatching multiple envelopes.
 *
 * Test with "I want a prize"
 */

bot.global.direct({
  is: 'plan meeting'
}, async (b) => {
  if (bot.adapters.message.name !== 'rocketchat-message-adapter') return
  b.envelope.write('Please review time zones in the room...')
  const { id, type } = b.message.user.room
  let room
  const q = { roomId: id }
  if (type === 'c') {
    room = await bot.adapters.message.api.get('channels.members', q, true)
  } else if (type === 'p') {
    room = await bot.adapters.message.api.get('groups.members', q, true)
  } else {
    return b.respond('Sorry, that only works in channels and private groups.')
  }
  const offsets = room.members
    .map((member) => member.utcOffset || undefined)
    .filter((offset) => !!offset)
  for (let utc of offsets) {
    b.envelope.payload.quickReply({
      text: `🌐 UTC ${utc}`,
      url: `https://www.timeanddate.com/worldclock/timezone/utc${utc}`
    })
  }
  b.respond()
})

/**
 * @todo This example requires PR #11811 to be merged. Room names are undefined.
*/
bot.global.text(/where am i/i, (b) => {
  const { name, type } = b.message.user.room
  switch (type) {
    case 'c': return b.respond(`You're in the #${name} public channel.`)
    case 'p': return b.respond(`You're in a private group called **${name}**.`)
    case 'l': return b.respond(`You're in a livechat channel.`)
    case 'd': return b.respond(`You're in a DM with me :hugging:`)
  }
}, {
    id: 'location-check'
  })


/**
 * Custom options can be added to the bot, with the full config utility of bBot,
 * allowing them to be defined as environment variables, command line args or
 * package.json attributes. Extend settings with a yargs option format.
 *
 * Try any of the following:
 *  - `node index.js --avatar <YOUR_AVATAR_URL>`
 *  - BOT_AVATAR=<YOUR_AVATAR_URL> in .env
 *  - `{ "bot": { "avatar": "<YOUR_AVATAR_URL>" } }` in package.json
 */
bot.settings.extend({
  avatar: {
    'type': 'string',
    'description': 'Set a custom avatar for your bot account profile'
  }
})

bot.middleware.hear((b, next, done) => {
  if (b.message.payload && b.message.user.room.type === 'd'){
    let attachment = b.message.payload.attachments[0]

    let file_title = (attachment.description) ? attachment.description : attachment.title
    let file_link = "https://chat.ispras.ru" + attachment.title_link

    auth().then(function (result) {
      add_file("Интересные_штуки", file_link, file_title)
        .then((result) => {
          b.respond("url: " + "http://nasredin.intra.ispras.ru/mediawiki/index.php/Интересные_штуки" +
            "\ntitle: " + file_title +
            "\nresult: " + result)
        })
    })
  }
  else next()
})

/**
 * The bot can access lower level methods of the Rocket.Chat SDK through the
 * message adapter, once it's connected. This example sets an avatar on login.
 *
 * Try replacing the avatar configured in package.json with your own.
 */
bot.events.on('started', () => {
  if (bot.adapters.message.name !== 'rocketchat-message-adapter') return
  if (bot.settings.get('avatar')) {
    bot.logger.info('Setting bot avatar')
    bot.adapters.message.api.post('users.setAvatar', {
      avatarUrl: bot.settings.get('avatar')
    })
  }
})

bot.start()